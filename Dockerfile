FROM node:15
WORKDIR /app
ADD . .
RUN npm install

RUN apt update -y 
RUN npm link cors
RUN npm install morgan
RUN npm install shelljs
RUN npm install express -y
RUN npm install axios -y 
RUN npm install express-basic-auth
RUN npm install sqlite
RUN npm install sqlite3 --save
RUN npm install jsonwebtoken
RUN npm install dotenv -y
RUN npm install aws-sdk -y
RUN npm i aws-sdk --save
RUN npm install md5 -y

EXPOSE 3000
CMD ["node", "index.js"]
