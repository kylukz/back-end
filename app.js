// IMPORT LIBS
const express = require('express')
const app = express()
const morgan = require('morgan')
var cors = require('cors')
// CONFIGURE LIBS
app.use(morgan('dev'))
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
// SET ROUTES
const crypto = require('./api/routes/Crypto')

////////////////////////////////////////////////////////
 
const SQLite = require('./api/models/SQLite');
const sqliteManager = new SQLite();

const jwt = require('jsonwebtoken');
app.post('/v1/loginAPI', (req, res, next) => { 
    var authLogin = req.body.user;
    var authPassw = req.body.password;
    // SQLITE AUTH
    let AuthUser = function () {
        return sqliteManager.selectUsuarioPorLogin(req.body.user);
    }
    let creden = AuthUser()
    var authOk = false;
    authOk = creden.then(async function (result) {
        if (result.length === 0) {
            return res.status(500).json({ message: 'Login inválido!' });
        } else {
            if (authLogin === result[0]['login'] && authPassw === result[0]['pass']) {
                // auth ok
                const id = result[0]['id']; //esse id viria do banco de dados
                const token = jwt.sign({ id }, process.env.SECRET, {
                    // Comente esta linha se quiser que nunca expire
                    expiresIn: 99999999 // expira em 3 anos
                });
                return res.json({ auth: true, token: token });
            }
            return res.status(500).json({ message: 'Login inválido!' });
        }
    })
})
// logout
app.post('/v1/logoutAPI', function (req, res) {
    res.json({ auth: false, token: null });
})
// Valida se contém token
function verifyJWT(req, res, next) {
    const token = req.headers['x-access-token'];
    if (!token) return res.status(401).json({ auth: false, message: 'Token not provided' });
    jwt.verify(token, process.env.SECRET, function (err, decoded) {
        if (err) return res.status(500).json({ auth: false, message: 'Token authentication failed.' });
        // se tudo estiver ok, salva no request para uso posterior
        req.userId = decoded.id;
        next();
    });
}
////////////////////////////////////////////////////////

// ENDPOINT DEFAULT
app.use('/v1/', verifyJWT, crypto)

app.use((req, res, next) => {
    const error = new Error('CronManager: RequestNotFoundException');
    error.status = 404
    next(error)
})
app.use((error, req, res, next) => {
    res.status(error.status || 500)
    res.json({
        error: {
            message: error.message
        }
    })
})
module.exports = app