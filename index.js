const http = require('http');
const app = require('./app');
require('dotenv').config();

init();

// =====================
// EXPRESS SERVER
// =====================
const port = process.env.PORT || 3000
const server = http.createServer(app)
server.listen(port, () => console.log(`Running in http://localhost:${port}/`))

function init() {
    //SQLITE INIT
    const SQLite = require('./api/models/SQLite');
    const sqliteManager = new SQLite();
    // Cria tabela de usuários se não existir
    var cria = sqliteManager.criaTabela();
    cria.then(async function(result) {
        sqliteManager.insertTabela('myuser', '123');
    })
}