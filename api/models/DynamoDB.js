var AWS = require("aws-sdk");

module.exports = class DynamoDB {

    constructor() {
        // Users of dashboard/front-end
        this.tableUsers = "users";
        // Dynamo's DB params
        this.region = "sa-east-1";
        this.endpoint = "http://dynamodb.sa-east-1.amazonaws.com";
        this.accessKeyId = "AKIAQOYTB3DSI7SE5IFT";
        this.secretAccessKey = "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";
        // Dynamo get connection
        AWS.config.update({
            "region": this.region,
            "endpoint": this.endpoint,
            "accessKeyId": this.accessKeyId,
            "secretAccessKey": this.secretAccessKey
        });
        this.dynamoDB = new AWS.DynamoDB.DocumentClient();
    }

    // Query/Select an user by id
    async getUserById(id) {
        return new Promise(resolve => {
            // Config params to connect in dynamo
            var query = this.dynamoDB.get({
                TableName: this.tableUsers,
                Key: {
                    id: id,
                },
            }).promise()
                .then(data => resolve(data.Item))
                .catch(console.error);
        })
    }

    // Query/Select an user by his login
    async getUserByLogin(login) {
        return new Promise(resolve => {
            // Config params to connect in dynamo
            var params = {
                TableName: this.tableUsers,
                FilterExpression: "login = :login",
                ExpressionAttributeValues: {
                    ':login': login
                }
            };
            this.dynamoDB.scan(params, function (err, data) {
                if (err) console.log(err, err.stack);
                else resolve(data.Items);
            });
        })
    }
}