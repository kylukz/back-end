const sqlite3 = require('sqlite3');
const sqlite = require('sqlite');


/**
 * Classe responsável por manipular sqlite
 * 
 * @author      Igor Maximo
 * @date        15/08/2021
 */
module.exports = class SQLite {

    // DROP TABLE
    async dropTabela() {
        const db = await this.abreConexao();
        await db.run('drop table if exists usuario');
    }

    // CRIA TABLE
    async criaTabela() {
        const db = await this.abreConexao();
        await db.run('create table if not exists usuario (id integer primary key, login text, pass text, se_inativo integer)');
    }

    // INSERT TABLE
    async insertTabela(usuario, senha) {
        const db = await this.abreConexao();
        await db.run('insert or ignore into usuario (login, pass, se_inativo) values (?, ?, ?)', [usuario, senha, 0]);
    }

    // UPDATE INATIVA USUÁRIO
    async inativaUsuario(id) {
        const db = await this.abreConexao();
        await db.run('update usuario set se_inativo = 1 where id = ' + id);
    }

    // SELECT TODOS ATIVO
    async selectTodosUsuarios() {
        const db = await this.abreConexao();
        const rows = await db.all('select * from usuario where se_inativo = 0');
        db.close();
        return rows;
    }

    async selectUsuarioPorLogin(login) {
        const db = await this.abreConexao();
        const rows = await db.all('select * from usuario where login = \'' + login + '\' and se_inativo = 0');
        db.close();
        //return rows;
        return new Promise(resolve => {
            resolve(rows);
        })
    }

    async selectUsuarioPorId(id) {
        const db = await this.abreConexao();
        const rows = await db.all('select * from usuario where id = ' + id + ' and se_inativo = 0');
        db.close();
        return rows;
    }

    async abreConexao() {
        return await sqlite.open({ filename: './mytacryptoauth.sqlite', driver: sqlite3.Database });
    }
}

