const DynamoDBConnection = require('../models/DynamoDB')
const LoginValidation = require('../validation/LoginValidation');


// Select user by id
exports.getUserById = async (req, res, next) => {
    try {
        const dynamoQuery = new DynamoDBConnection();
        const userDataById = await dynamoQuery.getUserById(parseInt(req.params.id));
        return res.json({ userDataById });
    } catch (err) {
        makeErrorMessage(err, next)
        return
    }
}
// Select user by login and auth it
exports.setAuthUser = async (req, res, next) => {
    try {
        const dynamoQuery = new DynamoDBConnection();
        const userDataByLogin = await dynamoQuery.getUserByLogin(req.body.login);
        // Validate and auth
        return new LoginValidation(req.body.login, req.body.password, userDataByLogin, res, next).validate();
    } catch (err) {
        makeErrorMessage(err, next)
        return
    }
}



exports.listCurrentExchange = async (req, res, next) => {
    try {
        console.log('lista');
    } catch (err) {
        makeErrorMessage(err, next)
        return
    }
}
 
// =================================================
// AUXILIARS FUNCTIONS 
// =================================================
function makeErrorMessage(err, next) {
    const error = new Error(err);
    error.status = 403
    next(error)
}