var md5 = require('md5');

module.exports = class LoginValidation {

    constructor(login, password, userDataByLogin, res, next) {
        this.login = login;
        this.password = password;
        this.userDataByLogin = userDataByLogin;
        this.res = res;
        this.next = next;
    }

    validate() {
        if (this.login.length < 5) {
            this.makeErrorMessage("Please, inform user!", this.next);
            return;
        }
        if (this.password.length < 3) {
            this.makeErrorMessage("Please, inform password!", this.next);
            return;
        }
        if (this.userDataByLogin.length > 0) {
            // Check if login exists
            if (this.userDataByLogin[0]['login'] !== this.login) {
                this.makeErrorMessage("Login not found!", this.next);
                return;
            }
            // Check if login exists
            if (this.userDataByLogin[0]['login'] === this.login && this.userDataByLogin[0]['password'] === md5(this.password)) {
                return this.res.json({
                    'status': true,
                    'msg': 'Authentication Successful!',
                    'userData': this.userDataByLogin
                });
            } else {
                this.makeErrorMessage("Wrong password!", this.next);
                return;
            }
        } else {
            this.makeErrorMessage("Login not found!", this.next);
            return;
        }
    }

    // =================================================
    // AUXILIARS FUNCTIONS 
    // =================================================
    makeErrorMessage(err, next) {
        const error = new Error(err);
        error.status = 403
        this.next(error)
    }
}