const express = require('express');
const router = express.Router();
const CryptoController = require('../controller/CryptoController')

router.post('/getUserById/:id', CryptoController.getUserById)
router.post('/setAuthUser', CryptoController.setAuthUser)
router.post('/listExchange', CryptoController.listCurrentExchange)

module.exports = router;